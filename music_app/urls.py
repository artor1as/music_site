from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from music_app import views


urlpatterns = [
    url(r'^tracks/$', views.TrackList.as_view()),
    url(r'^tracks/(?P<pk>[0-9]+)/$', views.TrackDetail.as_view()),
    url(r'^countries/$', views.CountryList.as_view()),
    url(r'^countries/(?P<pk>[A-Z]+)/$', views.CountryDetail.as_view()),
    url(r'^albums/$', views.AlbumList.as_view()),
    url(r'^albums/(?P<pk>[0-9]+)/$', views.AlbumDetail.as_view()),
    url(r'^artists/$', views.ArtistList.as_view()),
    url(r'^artists/(?P<pk>[0-9]+)/$', views.ArtistDetail.as_view()),
    url(r'^main_screen/$', views.MainScreenList.as_view()),
    url(r'^main_screen/(?P<pk>[0-9]+)/$', views.MainScreenDetail.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^track_likes/$', views.TrackLikeList.as_view()),
    url(r'^track_likes/(?P<pk>[0-9]+)/$', views.TrackLikeDetail.as_view()),
    url(r'^api-auth/', include('rest_framework.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
