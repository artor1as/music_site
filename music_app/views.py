from django.contrib.auth.models import User
from rest_framework import generics, permissions

from music_app.models import (Track, Country, Album, Artist, MainScreen,
                              TrackLike)
from music_app.serializers import (
    TrackSerializer, CountrySerializer, AlbumSerializer, ArtistSerializer,
    MainScreenSerializer, UserSerializer, TrackLikeSerializer)


class TrackList(generics.ListCreateAPIView):
    queryset = Track.objects.prefetch_related('artists').prefetch_related(
        'restricted_countries'
    )
    serializer_class = TrackSerializer
    permission_classes = (permissions.IsAdminUser,)


class TrackDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Track.objects.prefetch_related('artists').prefetch_related(
        'restricted_countries'
    )
    serializer_class = TrackSerializer
    permission_classes = (permissions.IsAdminUser,)


class CountryList(generics.ListCreateAPIView):
    queryset = Country.objects.prefetch_related()
    serializer_class = CountrySerializer
    permission_classes = (permissions.IsAdminUser,)


class CountryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (permissions.IsAdminUser,)


class AlbumList(generics.ListCreateAPIView):
    queryset = Album.objects.prefetch_related('artists')
    serializer_class = AlbumSerializer
    permission_classes = (permissions.IsAdminUser,)


class AlbumDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Album.objects.prefetch_related('artists')
    serializer_class = AlbumSerializer
    permission_classes = (permissions.IsAdminUser,)


class ArtistList(generics.ListCreateAPIView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
    permission_classes = (permissions.IsAdminUser,)


class ArtistDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
    permission_classes = (permissions.IsAdminUser,)


class MainScreenList(generics.ListCreateAPIView):
    queryset = MainScreen.objects.select_related('artist')
    serializer_class = MainScreenSerializer
    permission_classes = (permissions.IsAdminUser,)


class MainScreenDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MainScreen.objects.select_related('artist')
    serializer_class = MainScreenSerializer
    permission_classes = (permissions.IsAdminUser,)


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TrackLikeList(generics.ListAPIView):
    queryset = TrackLike.objects.prefetch_related('user')
    serializer_class = TrackLikeSerializer
    permission_classes = (permissions.IsAdminUser,)


class TrackLikeDetail(generics.RetrieveAPIView):
    queryset = TrackLike.objects.prefetch_related('user')
    serializer_class = TrackLikeSerializer
    permission_classes = (permissions.IsAdminUser,)
