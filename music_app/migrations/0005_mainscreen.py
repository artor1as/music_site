# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-25 13:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('music_app', '0004_auto_20171010_1450'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainScreen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveIntegerField(unique=True)),
                ('artist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='music_app.Artist')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
