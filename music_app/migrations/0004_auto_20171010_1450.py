# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-10 14:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('music_app', '0003_auto_20171009_1118'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'ordering': ['id']},
        ),
        migrations.AlterField(
            model_name='country',
            name='name',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
