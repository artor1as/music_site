import datetime
from django.db import models
from django.contrib.auth.models import User


YEAR_CHOICES = [(r, r) for r in range(1960, datetime.date.today().year+1)]


class Country(models.Model):
    id = models.CharField(max_length=2, primary_key=True, unique=True)
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return '{0}, {1}'.format(self.id, self.name)

    class Meta:
        ordering = ['id']


class Track(models.Model):
    name = models.CharField(max_length=100)
    artists = models.ManyToManyField('Artist', related_name='tracks')
    album = models.ForeignKey('Album', on_delete=models.CASCADE,
                              related_name='tracks')
    path = models.CharField(max_length=150, default='/home/music/')
    restricted_countries = models.ManyToManyField(
        'Country',
        related_name='tracks',
        default=Country.objects.all())

    def __str__(self):
        return self.name


class Album(models.Model):
    name = models.CharField(max_length=100)
    artists = models.ManyToManyField('Artist', related_name='artists')
    year = models.IntegerField(
        'year',
        choices=YEAR_CHOICES,
        default=datetime.datetime.now().year
    )

    def __str__(self):
        return self.name


class Artist(models.Model):
    name = models.CharField(max_length=90)
    bio = models.TextField(max_length=4000)

    def __str__(self):
        return self.name


class MainScreen(models.Model):
    artist = models.ForeignKey('Artist', related_name='main_screens')
    order = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return '{}'.format(self.artist)

    class Meta:
        ordering = ['order']


class TrackLike(models.Model):
    user = models.ForeignKey(User)
    track = models.ForeignKey('Track')

    def __str__(self):
        return '{0} - {1}'.format(self.user, self.track)

    class Meta:
        unique_together = ('user', 'track')
