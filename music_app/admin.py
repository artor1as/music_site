from django.contrib import admin

from .models import Album, Artist, Track, Country, MainScreen, TrackLike


class TrackInLine(admin.TabularInline):
    model = Track
    extra = 0


class AlbumAdmin(admin.ModelAdmin):
    fields = ['artists', 'name', 'year']
    list_display = ('name', 'year')
    inlines = [TrackInLine]


class ArtistAdmin(admin.ModelAdmin):
    pass


class MainScreenAdmin(admin.ModelAdmin):
    list_display = ('artist', 'order')


admin.site.register(Album, AlbumAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Track)
admin.site.register(Country)
admin.site.register(MainScreen, MainScreenAdmin)
admin.site.register(TrackLike)
