from django.contrib.auth.models import User
from rest_framework import serializers

from music_app.models import (Track, Album, Artist, Country,
                              MainScreen, TrackLike)


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = ('id', 'name', 'path', 'artists', 'album',
                  'restricted_countries')


class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = ('id', 'name', 'artists', 'year')


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('id', 'name', 'bio')


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name')


class MainScreenSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainScreen
        fields = ('id', 'artist', 'order')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser')


class TrackLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrackLike
        fields = ('id', 'user', 'track')
