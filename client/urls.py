from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from client import views


urlpatterns = [
    url(r'^discovery/$', views.DiscoveryAPI.as_view(),
        name='mainscreen-list'),
    url(r'^discovery/$', views.DiscoveryAPI.as_view(),
        name='mainscreen-list'),
    url(r'^artist/(?P<pk>[0-9]+)/albums/$',
        views.AlbumAPI.as_view(), name='artist-albums-detail'),
    url(r'^artist/(?P<pk>[0-9]+)/tracks/$',
        views.TrackAPI.as_view(), name='artist-tracks-detail'),
    url(r'^users/$', views.UserAPI.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/update/$', views.UserUpdateAPI.as_view()),
    url(r'^signup/$', views.UserCreateAPI.as_view()),
    url(r'^likes/tracks/(?P<pk>[0-9]+)/$', views.TrackLikesAPI.as_view()),
    url(r'^likes/users/(?P<pk>[0-9]+)/$', views.UserLikesAPI.as_view()),
    url(r'^likes/$', views.LikeCreateListAPI.as_view()),
    url(r'^likes/(?P<pk>[0-9]+)/$', views.LikeCreateDeleteAPI.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
