from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.generics import get_object_or_404
from rest_framework.reverse import reverse

from music_app.models import (Artist, Album, Track, MainScreen, TrackLike,
                              Country)


class ArtistGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('id', 'name',)


class AlbumInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = ('id', 'name', 'year')


class TrackInfoSerializer(serializers.ModelSerializer):
    path = serializers.SerializerMethodField()

    def get_path(self, obj):
        try:
            country_id = self.context['view'].kwargs['country_id']
            if country_id in obj.restricted_countries.values_list('id',
                                                                  flat=True):
                return obj.path
        except KeyError:
            return obj.path

    class Meta:
        model = Track
        fields = ('id', 'name', 'path')


class TrackGeneralSerializer(TrackInfoSerializer):
    album = AlbumInfoSerializer()

    class Meta:
        model = Track
        fields = ('id', 'name', 'album', 'path')


class AlbumGeneralSerializer(AlbumInfoSerializer):
    tracks = TrackInfoSerializer(many=True)

    class Meta:
        model = Album
        fields = ('id', 'name', 'year', 'tracks')


class TrackLikeInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrackLike
        fields = ('id', 'track')


class TrackGeneralSerializerVersion2(TrackGeneralSerializer):
    like_count = serializers.SerializerMethodField()

    def get_like_count(self, obj):
        like_query = TrackLike.objects.filter(track_id=obj.id).count()
        return like_query

    class Meta:
        model = Track
        fields = ('id', 'name', 'album', 'path', 'like_count')


class TrackGeneralSerializerVersion3(TrackGeneralSerializerVersion2):
    class Meta:
        model = Track
        fields = ('id', 'name', 'album', 'path', 'like_count')


class MainScreenGeneralSerializer(serializers.ModelSerializer):
    artist = ArtistGeneralSerializer()
    albums_link = serializers.SerializerMethodField()
    tracks_link = serializers.SerializerMethodField()

    def build_link(self, obj, view_name):
        request = self.context['request']
        url_kwargs = {
            'pk': obj.artist_id,
        }
        return request.build_absolute_uri(reverse(view_name,
                                                  kwargs=url_kwargs))

    def get_albums_link(self, obj):
        return self.build_link(view_name='v1:artist-albums-detail', obj=obj)

    def get_tracks_link(self, obj):
        return self.build_link(view_name='v1:artist-tracks-detail', obj=obj)

    class Meta:
        model = MainScreen
        fields = ('id', 'order', 'artist', 'albums_link', 'tracks_link')


class MainScreenGeneralSerializerVersion2(MainScreenGeneralSerializer):
    albums_link = serializers.SerializerMethodField()
    tracks_link = serializers.SerializerMethodField()

    def get_albums_link(self, obj):
        return self.build_link(view_name='v2:artist-albums-detail', obj=obj)

    def get_tracks_link(self, obj):
        return self.build_link(view_name='v2:artist-tracks-detail', obj=obj)

    class Meta:
        model = MainScreen
        fields = ('id', 'order', 'artist', 'albums_link', 'tracks_link')


class MainScreenGeneralSerializerVersion3(MainScreenGeneralSerializerVersion2):
    albums_link = serializers.SerializerMethodField()
    tracks_link = serializers.SerializerMethodField()

    def build_link(self, obj, view_name):
        request = self.context['request']
        view = self.context['view']
        get_object_or_404(Country.objects.all(),
                          id=view.kwargs['country_id'])
        url_kwargs = {
            'pk': obj.artist_id,
            'country_id': view.kwargs['country_id']
        }
        return request.build_absolute_uri(reverse(view_name,
                                                  kwargs=url_kwargs))

    def get_albums_link(self, obj):
        return self.build_link(view_name='v3:artist-albums-detail', obj=obj)

    def get_tracks_link(self, obj):
        return self.build_link(view_name='v3:artist-tracks-detail', obj=obj)

    class Meta:
        model = MainScreen
        fields = ('id', 'order', 'artist', 'albums_link', 'tracks_link')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    email = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'first_name',
                  'last_name')

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserUpdateSerializer(UserSerializer):
    password = serializers.CharField(write_only=True, allow_blank=True)
    email = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'first_name',
                  'last_name')

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'username':
                continue
            if attr == 'password' and not value or len(value) <= 8:
                continue
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance
