from django.contrib.auth.models import User
from rest_framework import generics, permissions
from rest_framework.generics import get_object_or_404

from music_app.models import Album, MainScreen, Track, TrackLike, Country
from client.permissions import OnlyUserCanOrReadOnly
from client.serializers import (
    AlbumGeneralSerializer,
    MainScreenGeneralSerializer,
    MainScreenGeneralSerializerVersion2,
    MainScreenGeneralSerializerVersion3,
    TrackGeneralSerializer,
    TrackGeneralSerializerVersion2,
    TrackGeneralSerializerVersion3,
    TrackLikeInfoSerializer,
    UserSerializer,
    UserUpdateSerializer,
    )


class AlbumAPI(generics.ListAPIView):
    serializer_class = AlbumGeneralSerializer

    def get_queryset(self):
        if self.request.version == 'v3':
            get_object_or_404(Country.objects.all(),
                              id=self.kwargs['country_id'])
        artist_id = self.kwargs['pk']
        return Album.objects.filter(artists__id=artist_id)


class DiscoveryAPI(generics.ListAPIView):
    queryset = MainScreen.objects.select_related('artist')

    def get_serializer_class(self):
        if self.request.version == 'v2':
            return MainScreenGeneralSerializerVersion2
        elif self.request.version == 'v3':
            return MainScreenGeneralSerializerVersion3
        return MainScreenGeneralSerializer


class TrackAPI(generics.ListAPIView):
    def get_queryset(self):
        if self.request.version == 'v3':
            get_object_or_404(Country.objects.all(),
                              id=self.kwargs['country_id'])
        artist_id = self.kwargs['pk']
        return Track.objects.filter(artists__id=artist_id).select_related(
            'album')

    def get_serializer_class(self):
        if self.request.version == 'v2':
            return TrackGeneralSerializerVersion2
        elif self.request.version == 'v3':
            return TrackGeneralSerializerVersion3
        return TrackGeneralSerializer


class TrackLikesAPI(generics.ListAPIView):
    serializer_class = TrackLikeInfoSerializer

    def get_queryset(self):
        track_id = self.kwargs['pk']
        return TrackLike.objects.filter(track_id=track_id)


class UserLikesAPI(generics.ListAPIView):
    serializer_class = TrackLikeInfoSerializer

    def get_queryset(self):
        user_id = self.kwargs['pk']
        return TrackLike.objects.filter(user_id=user_id)


class LikeCreateListAPI(generics.ListCreateAPIView, ):
    queryset = TrackLike.objects.all()
    serializer_class = TrackLikeInfoSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LikeCreateDeleteAPI(generics.CreateAPIView,
                          generics.DestroyAPIView,
                          generics.RetrieveAPIView):
    queryset = TrackLike.objects.all()
    serializer_class = TrackLikeInfoSerializer
    permission_classes = (OnlyUserCanOrReadOnly,)


class UserAPI(generics.ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAdminUser,)


class UserCreateAPI(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class UserUpdateAPI(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
    permission_classes = (OnlyUserCanOrReadOnly,)
